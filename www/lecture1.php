<!--
<?php
$txt = "Hello World!";
$number = 10;

echo $txt ."<br>";
echo $number;

echo "<h4>This is a simple heading.</h4>";
echo "<h4 style='color: Red;'>This is heading with style.</h4>";

$num = 123456789;
$colors = array("Red","Green","Blue");

$a = 123;
var_dump($a);
echo "<br>";

$b = -123;
var_dump($b);
echo "<br>";

$c = 0x1A;
var_dump($c);
echo "<br>";

$d = 0123;
var_dump($d);

var_dump($colors);
echo "<br>";

$colors_code = array(
	"Red" => "#ff0000",
	"Green" => "#00ff00",
	"Blue" => "#0000ff"
);
var_dump($colors_code);

class greeting{
	public $str = "Hello";
	function show_greeting(){
		return $this->str;
	}

}
$message = new greeting;
var_dump($message);
echo "<br>".$message->show_greeting();

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<?php
if (isset($_REQUEST["name"])) {
	echo "<p>Hi, ". $_REQUEST["name"] . "</p>";
}
?>
<form method="POST" action="<?php echo $_SERVER["PHP_SELF"];?>">
	<label for="inputName">Name:</label>
	<input type="text" name="name" id="inputName">
	<input type="submit" value="Submit">
</form>
</body>
</html>
-->